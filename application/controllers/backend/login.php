<?php 

class login extends Backend_Info_Controller 
{
	public function __construct()
	{
		parent::__construct("backend");			
	}
	
	public function index()
	{	
		if($_POST)
		{ 
			if($this->session->userdata("cap_word") == $this->input->post("code"))
			{
				$this->load->model("backend/cms_user_m");
				$where["account"] = $this->input->post('account');
				$admin = $this->cms_user_m->get_by($where);
				if($admin){
					if($admin->password_admin == $this->input->post('password')){
						if($admin->status == 1)
						{
							$this->session->set_userdata('user',$admin);
							redirect("backend/world");						
						}else
							$this->data["msg"] = "此帳號尚未啟用";
					}else
						$this->data["msg"] = "帳號密碼錯誤";
				}else
					$this->data["msg"] = "帳號密碼錯誤";
			}else
				$this->data["msg"] = "驗證碼錯誤";
		}
		$this->cap();
		$this->load->view('backend/login/index',$this->data);	
	}
	
	public function cap()
	{
		$this->load->helper('captcha');
		
		$vals = array(
	    'img_path'	 => './upload/captcha/',
	    'img_url'	 => './upload/captcha/',
	    'img_width'	 => 100,
	    'img_height' => 38,
	    );
		
		$this->data["cap"] = create_captcha($vals);
		$this->session->set_flashdata('cap_word', $this->data["cap"]["word"]);
		$this->session->set_userdata('cap_word', $this->data["cap"]["word"]);
	}
	
	public function logout()
	{
		$this->session->unset_userdata('user');
		$this->session->unset_userdata('admin');
		$this->data["msg"] = "登出成功";
		redirect("backend");
	}
}