<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Kode is a Premium Bootstrap Admin Template, It's responsive, clean coded and mobile friendly">
  <meta name="keywords" content="bootstrap, admin, dashboard, flat admin template, responsive," />
  <title>後台管理系統</title>

  <!-- ========== Css Files ========== -->
  <link href="<?=base_url("/contents/backend/css/root.css")?>" rel="stylesheet">
  <link rel="Shortcut Icon" type="image/x-icon" href="<?=base_url("contents/frontend/assets/images/favicon.ico")?>" />
  
  <!-- === Icons === -->
  <link rel="Shortcut Icon" type="image/x-icon" href="<?=base_url("contents/frontend/assets/img/favicon.ico")?>" />
  
  <style type="text/css">
    body{background: #F5F5F5;}
  </style>
  </head>
  <body>
    <!-- Start Page Loading -->
    <div class="loading"><img src="<?=base_url("contents/backend/img/loading.gif")?>" alt="loading-img"></div>
    <!-- End Page Loading -->
    <div class="login-form">
      <form method="post">
        <div class="top">
          <!-- <img src="<?=base_url("contents/backend")?>/img/logo.gif" alt="icon" > -->
          <h1>後台管理系統</h1>
        </div>
        <div class="form-area">
          <div class="group">
            <input type="text" class="form-control" placeholder="Username" id="account" name="account">
            <i class="fa fa-user"></i>
          </div>
          <div class="group">
            <input type="password" class="form-control" placeholder="Password" id="password" name="password">
            <i class="fa fa-key"></i>
          </div>
          <div class="group">
            <input type="text" class="" placeholder="code" id="code" name="code" style="width: 62%; padding-left: 38px; height: 40px;">
            <?=$cap["image"]?>
            
            <i class="fa fa-lock"></i>
          </div>
          <button type="button" class="btn btn-default btn-block submit_login">登入</button>
        </div>
      </form>
    </div>

    <? $this->load->view("backend/partials/script");?>
    <script>
        $(document).ready(function(){
          if("<?=$msg?>")
            swal({   title: "<?=$msg?>",   timer: 3000 });

          $("#code_image").css("margin-bottom", 3);
    
          $("#code").keypress(function(event){
            if(event.keyCode == 13){
              Verification();
            }
          });

          $(".submit_login").click(function()
          {      
            Verification();
          });
        });

        function Verification(){
            if($("#account").val() == "")
            {
                $("#account").focus();
                swal({   title: "帳號不能空白",   timer: 3000 });
                return false;
            }  
            if($("#password").val() == "")
            {
                $("#password").focus();
                swal({   title: "密碼不能空白",   timer: 3000 });
                return false;
            }  
            if($("#code").val() == "")
            {
                $("#code").focus();
                swal({   title: "驗證碼不能空白",   timer: 3000 });
                return false;
            }
            $('form').submit();
        }
    </script>
</body>
</html>