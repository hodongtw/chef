<!DOCTYPE html>
<html lang="en">
<? $this->load->view("backend/partials/meta"); ?>

<body>
  <!-- Start Page Loading -->
  <!-- End Page Loading -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START TOP -->
  <? $this->load->view("backend/partials/top"); ?>
  <!-- END TOP -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START SIDEBAR -->
  <? $this->load->view("backend/partials/sidebar"); ?>
  <!-- END SIDEBAR -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START CONTENT -->
  <div class="content">
    <!-- Start Page Header -->
    <div class="page-header">
      <h1 class="title">世界排行管理</h1>
      <ol class="breadcrumb">
        <li><a href="<?=site_url("backend/world")?>">內容管理</a></li>
        <li class="active">世界排行管理</li>
      </ol>
      <!-- Start Page Header Right Div -->
      <div class="right">
        <div class="btn-group" role="group" aria-label="...">
          <a href="<?=site_url($site_root."/modify")?>" class="btn btn-light"><i class="fa fa-plus"></i></a>
        </div>
      </div>
      <!-- End Page Header Right Div -->
    </div>
    <!-- End Page Header -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START CONTAINER -->
    <div class="container-padding">              
      <? if($msg):?>
        <div class="kode-alert kode-alert-icon kode-alert-click alert3-light">
          <i class="fa fa-check"></i>
          <a href="#" class="closed">&times;</a>
          <?=$msg?>
        </div>
      <? endif;?>
      <!-- <form enctype="multipart/form-data" action="<?=base_url("backend/world")."/import_xls"?>" method="post">
        <div class="row" style="margin-bottom: 10px;">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                      <label style="float: left; margin-right: 3em;" for="import_xls" class="control-label">匯入Excel:<br/></label>
              <div style="float: left;">
                <input style="float: left; margin-right: 3em;" type="file" name="import_xls" size="20"/>
                <input style="float: left; border-radius: 5px; border: 1px solid #b4b4b4; background-color: #eeeeee; margin-top: -3px; margin-right: 2em;" type="submit" value="上傳" />
                      </div>
            </div>
          </div>
        </div> 
      </form> -->
      <!-- Start Row -->
      <div class="row">
        <!-- Start Panel -->
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-body table-responsive">
              <? if($pagination):?><ul class="pagination"><?=$pagination?></ul><? endif;?>
              <table class="table table-hover" id="dt">
                <thead>
                  <tr>
                    <td style="width: 6%">Image</td>
                    <td style="width: 7%">World 50</td>
                    <td style="width: 20%">Restaurant</td>
                    <!-- <td style="width: 40%">姓名</td> -->
                    <td style="width: 10%">Country</td>
                    <td style="width: 10%">Regoin</td>
                    <td style="width: 15%">Style of food</td>
                    <td style="width: 7%">Michelin</td>
                    <td style="width: 7%">Speech</td>
                    <td style="width: 7%">Guest Chef</td>
                    <td style="width: 7%">Available Days</td>
                    <td style="width: 7%">Price /1000k USD</td>
                    <td style="width: 10%">Action</td>      
                  </tr>
                </thead>
                <tbody>
                <?foreach($items as $item):?>
                  <tr  data="<?=$item->id?>">
                    <?$image = json_decode($item->image)[0];?>
                    <?if($image):?>
                      <td><img style="max-height: 100px; max-width: 100px;" src="<?=base_url("upload/world/$image")?>"></td>
                    <?else:?>
                      <td></td>
                    <?endif;?>
                    <td><?=$item->world?></td>
                    <td><?=$item->restaurant?></td>
                    <!-- <td><?=$item->name?></td> -->
                    <td><?=$item->country?></td>
                    <td><?=$item->regoin?></td>
                    <td><?=$item->style?></td>
                    <td><?=$item->michelin?></td>
                    <td><?=$item->speech ? "Y" : "N"?></td>
                    <td><?=$item->guest ? "Y" : "N"?></td>
                    <td><?=$item->day?></td>
                    <td><?=$item->price?></td>
                    <td>
                      <a href="<?=site_url($site_root.'/modify/'.$item->id)?>" class="btn btn-light btn-sm">編輯</a>
                    </td>
                  </tr>
                <?endforeach;?>
                </tbody>
              </table>
            <? if($pagination):?><ul class="pagination"><?=$pagination?></ul><? endif;?>
            </div>
          </div>
        </div>
        <!-- End Panel -->
      </div>
      <!-- End Row -->
    </div>
    <!-- END CONTAINER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- Start Footer -->
    <? $this->load->view("backend/partials/footer");?>
    <!-- End Footer -->
  </div>
  <!-- End Content -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START SIDEPANEL -->
  <!-- END SIDEPANEL -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <? $this->load->view("backend/partials/script");?>
  <script >    
    $(document).ready(function() {
      $('#dt').DataTable({
          "paging": false,
      } );
    });
  </script>
</body>

</html>