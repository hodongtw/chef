<div class="sidebar clearfix">
  	<ul class="sidebar-panel nav">
	    <li><a href="#"><span class="icon color7"><i class="fa fa-cog"></i></span>系統管理<span class="caret"></span></a>
			<ul>      	  
				<li><a href="<?=site_url("backend/cms_user/")?>">系統帳號管理</a></li>
				<li><a href="<?=site_url("backend/cms_system/modify/1")?>">系統資訊設定</a></li>
			</ul>
		</li>
	    <li><a href="#"><span class="icon color7"><i class="fa fa-newspaper-o"></i></span>內容管理<span class="caret"></span></a>
			<ul>      	  
				<li><a href="<?=site_url("backend/taiwan")?>">台灣排行</a></li>
				<li><a href="<?=site_url("backend/asia")?>">亞洲排行</a></li>
				<li><a href="<?=site_url("backend/world")?>">世界排行</a></li>
			</ul>
		</li>
  	</ul>
</div>