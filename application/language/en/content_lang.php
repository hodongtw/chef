<?php

// INDEX
$lang['RESERVATION'] = "RESERVATION";
$lang['ABOUT'] = "ABOUT";
$lang['MENU'] = "MENU";
$lang['TEAM'] = "TEAM";
$lang['PRESS'] = "PRESS";

// MUNE
$lang['MUNE'] = "In creating our menu, we respect the uniqueness that each ingredient offers, and seek to capture that essence of each ingredient <br>while complementing its flavor with a special touch from our expert culinary team.";

// ABOUT
$lang['CONCEPT'] = "Here at Orchid, we serve Modern French Cuisine that is shaped by our sense of Time and Place. With this cooking philosophy serving as the starting point for every dish we create, we use only the freshest and seasonal ingredients sourced both locally and from around the world. As a result, we proudly present to you a menu that is balanced, layered, and memorable.";
$lang['INSPIRATION'] = "Inspiration comes from being attentive to life, and it can come at anytime, anywhere, from anybody, and for any reason. Inspiration infuses into our dishes creativity and emotion, thus giving each dish its unique identity and character. Even though our menu is anchored by our deep respect for traditional French cuisine, we also like to infuse our dishes with Taiwanese sensibility. This fusion has allowed us to develop a style of cooking that is modern yet pure.";
$lang['SPACE'] = "The theme of our interior design is based on a modern take on Zen philosophy, whilst reflecting our French and Taiwanese fusion. Utilizing a color palette of black and gold accented with works of calligraphy, and incorporating an eight meter high ceiling space, we bring you a room that is spacious and modestly luxe, perfect for an enjoyable dining experience.";

// TEAM
$lang['CHEF'] = "Chef Gildas Périn’s style of cooking is built upon his mastery of French cuisine fundamentals and his constant exploration of Taiwanese ingredients. By infusing a part of his Taiwanese soul into his love for French traditions, each flavorful dish he creates boldly expresses his unique take on Modern French cuisine. <br><br>
Born in Normandy, Chef Gildas Périn laid down his culinary ambitions at the tender age of 12. He entered into the culinary school in France to receive the rigorous training that would transform passion into skill. Upon graduation, he has interned and worked for a number of prestigious restaurants, including Cordeillan Bages, Royal Barriere in Deauville, Westminster Hotel in Northen France, and A contre Sens in Caen. Chef Gildas Périn established himself as a force to be reckoned with when he won second place at the Creations et Saveurs competition in 2014, and won first place at the Challenge Culinaire du President de La Republique in 2015, which was hosted by MOF awarded Chef Guillaume Gomez. <br><br>
In his quest to better his culinary expertise, Chef Gildas Périn was fortunate to have met two mentors who imparted to him invaluable insight and experience. The first mentor was Chef Nicolas Sale of the Ritz Paris, whom Gildas Périn assisted in managing two restaurants (respectively two Michelin stars and one Michelin star). In 2017, Gildas Périn met his second mentor in six Michelin star Chef Yannick Alleno. Chef Yannick Alleno invited Gildas Périn to be his sous chef at STAY Taipei, and during this period, Chef Yannick Alleno ingrained in Gildas Périn the concept that great food comes from great ingredients. Therefore, they would tirelessly seek out top quality ingredients, and once found, they would then work even more vigorously to explore the multitude of ways such an ingredient could be prepared. Their meticulous experimentation would not stop until they find what they deem to be the best way to prepare such an ingredient, and then would they start to consider how to enrich this ingredient with other components and sauces. <br><br>";
$lang['CULINARY'] = "The culinary team at Orchid is one that is defined by undeniable passion, talent, and diligence. Each team member believes in the sanctity of ingredients, the life that they hold, the stories that they can convey. Orchid’s culinary team is always striving to learn more, to create more, and to inspire more through their exploration of different ingredients, and to bring their best to the table for your satisfaction.";
$lang['SERVICE'] = "The service team at Orchid is attentive, thoughtful, and above all, professional. Every member regards genuine human interaction as the most important basis for their service, and aspires to elevate your dining experience into one that you can trust to have your best interests at heart.";
