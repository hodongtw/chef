<!DOCTYPE html>
<html lang="en">
<? $this->load->view("backend/partials/meta"); ?>

<body>
  <!-- Start Page Loading -->
  <!-- End Page Loading -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START TOP -->
  <? $this->load->view("backend/partials/top"); ?>
  <!-- END TOP -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START SIDEBAR -->
  <? $this->load->view("backend/partials/sidebar"); ?>
  <!-- END SIDEBAR -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START CONTENT -->
  <div class="content">
    <!-- Start Page Header -->
    <div class="page-header">
      <h1 class="title">系統帳號管理</h1>
      <ol class="breadcrumb">
        <li><a href="<?=site_url("backend/cms_user")?>">系統管理</a></li>
        <li class="active">系統帳號管理</li>
      </ol>
      <!-- Start Page Header Right Div -->
      <div class="right">
        <div class="btn-group" role="group" aria-label="...">
          <a href="<?=site_url($site_root."/modify")?>" class="btn btn-light"><i class="fa fa-plus"></i></a>
        </div>
      </div>
      <!-- End Page Header Right Div -->
    </div>
    <!-- End Page Header -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START CONTAINER -->
    <div class="container-padding">              
      <? if($msg):?>
        <div class="kode-alert kode-alert-icon kode-alert-click alert3-light">
          <i class="fa fa-check"></i>
          <a href="#" class="closed">&times;</a>
          <?=$msg?>
        </div>
      <? endif;?>
      <!-- Start Row -->
      <div class="row">
        <!-- Start Panel -->
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-body table-responsive">
              <? if($pagination):?><ul class="pagination"><?=$pagination?></ul><? endif;?>
              <table class="table table-hover">
                <thead>
                  <tr>
                    <td style="width: 20%">名稱</td>
                    <td style="width: 15%">帳號</td>
                    <td style="width: 40%">E-mail</td>
                    <td style="width: 10%">狀態</td>
                    <td style="width: 15%">功能</td>
                  </tr>
                </thead>
                <tbody>
                <?foreach($items as $item):?>
                  <tr  data="<?=$item->id?>">
                    <td><?=$item->name?></td>
                    <td><?=$item->account?></td>
                    <td><?=$item->email?></td>
                    <td class="hidden-xs"><?=$item->status==1?"啟用":"停用"?></td>
                    <td>
                      <a href="<?=site_url($site_root.'/modify/'.$item->id)?>" class="btn btn-light btn-sm">編輯</a>
                    </td>
                  </tr>
                <?endforeach;?>
                </tbody>
              </table>
            <? if($pagination):?><ul class="pagination"><?=$pagination?></ul><? endif;?>
            </div>
          </div>
        </div>
        <!-- End Panel -->
      </div>
      <!-- End Row -->
    </div>
    <!-- END CONTAINER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- Start Footer -->
    <? $this->load->view("backend/partials/footer");?>
    <!-- End Footer -->
  </div>
  <!-- End Content -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START SIDEPANEL -->
  <!-- END SIDEPANEL -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <? $this->load->view("backend/partials/script");?>
</body>

</html>