<?php if (! defined('BASEPATH')) exit('No direct script access');

class MY_Controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model("backend/cms_system_m");
		$this->data["site_sys"] = $this->cms_system_m->get(1);		
	}
	
	public function get_rand_word()
	{
		$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

		$str = '';
		for ($i = 0; $i < 10; $i++)
		{
			$str .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
		}

		return $str;
	}
	
	public function get_html_select_data($object,$key,$value)
	{
		$result = array();
		
		foreach($object as $item)
		{
			$result[$item->$key] = $item->$value;
		}
		
		return $result;
	}
	
	public function get_resize_img($url,$width,$heigh)
	{
		if($url)
		{
			$filename = str_replace(substr($url, $this->GetExtLen($url)), substr($url, $this->GetExtLen($url)), $url);
			$checkfilename = str_replace(substr($url, $this->GetExtLen($url)), "_thumb".substr($url, $this->GetExtLen($url)), $url);
			
			if(file_exists($checkfilename))
				return  "/".$checkfilename;


			$config['image_library'] = 'gd2';
			$config['source_image']	= "./".$url;
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = TRUE;
			$config['width'] = $width;
			$config['height'] = $heigh;
			$config['new_image'] = "./".$filename;
			
			$this->image_lib->clear();
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
	
			return str_replace("./","/",$this->image_lib->full_dst_path);
		}
		else
			return "";
	}
	
	public function get_resize_img_width($url,$width)
	{
		if($url)
		{
			$filename = str_replace(substr($url, $this->GetExtLen($url)), substr($url, $this->GetExtLen($url)), $url);
			$checkfilename = str_replace(substr($url, $this->GetExtLen($url)), "_thumb".substr($url, $this->GetExtLen($url)), $url);
			if(file_exists($checkfilename))
				return  "/".$checkfilename;
			$config['image_library'] = 'gd2';
			$config['source_image']	= "./".$url;
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = TRUE;
			$config['width'] = $width;
			$config['new_image'] = "./".$filename;
			$config['master_dim'] = "width";
			$this->image_lib->clear();
			$this->image_lib->initialize($config);
			$this->image_lib->resize();

			return str_replace("./","/",$this->image_lib->full_dst_path);
		}
		else
			return "";
	}
	
	public function get_resize_img_height($url,$heigh)
	{
		if($url)
		{
			$filename = str_replace(substr($url, $this->GetExtLen($url)), "_" . $heigh . substr($url, $this->GetExtLen($url)), $url);
			$checkfilename = str_replace(substr($url, $this->GetExtLen($url)), "_".$heigh."_thumb".substr($url, $this->GetExtLen($url)), $url);
			if(file_exists($checkfilename))
				return  "/".$checkfilename;
			$config['image_library'] = 'gd2';
			$config['source_image']	= "./".$url;
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = TRUE;
			$config['height'] = $heigh;
			$config['new_image'] = "./".$filename;
			$config['master_dim'] = "height";
			$this->image_lib->clear();
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
	
			return str_replace("./","/",$this->image_lib->full_dst_path);
		}
		else
			return "";
	}
	
	function GetExtLen($url)
	{
		$paths = explode("/", $url);
		$last_path = array_pop($paths);
		$ext = explode(".", $last_path);	
		$file_ext = array_pop($ext);
		$sublen = (strlen($file_ext)+1) * -1;
			
		return $sublen;
	}

	/**　寄信功能
	 * Wrapper to __construct for when loading class is a superclass to a regular
	 * controller, i.e. - extends Base not extends Controller.
	 *
	 * @return if success return true else return message
	 * @param to should be a string explode by ','
	 * @author G
	 */
	public function sendEmail($subject, $message)
	{
		$this->load->model("backend/cms_user_m");
		$cms_user = $this->cms_user_m->get(1);	

		$this->email->from("a6011034@hodong.com.tw", "維納斯SPA");
		$emails = explode(",", $cms_user->email);
		$this->email->to($emails); 
		$this->email->subject($subject);
		$this->email->message($message);
		
		if(!$this->email->send())
			log_message("error","寄送失敗：" . $this->email->print_debugger());
	}
}

class Frontend_Controller extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->data["err_msg"] =  $this->session->flashdata('err_msg');
		$this->data["msg"] =  $this->session->flashdata('msg');
		
		//初始化分頁設定
		$this->data["pagination_config"] = array();
		$this->data["pagination_config"]['num_links'] = 4;
		$this->data["pagination_config"]['cur_tag_open'] = '<li class="active"><a>'; 
		$this->data["pagination_config"]['cur_tag_close'] = '</a></li>';  
		$this->data["pagination_config"]['num_tag_open'] = '<li>'; 
		$this->data["pagination_config"]['num_tag_close'] = '</li>'; 
		$this->data["pagination_config"]['next_tag_open'] = '<li>'; 
		$this->data["pagination_config"]['next_tag_close'] = '</li>'; 
		$this->data["pagination_config"]['prev_tag_open'] = '<li>'; 
		$this->data["pagination_config"]['prev_tag_close'] = '</li>'; 
		$this->data["pagination_config"]['first_link'] = '<<'; 
		$this->data["pagination_config"]['first_tag_open'] = '<li>'; 
		$this->data["pagination_config"]['first_tag_close'] = '</li>'; 
		$this->data["pagination_config"]['last_link'] = '>>'; 
		$this->data["pagination_config"]['last_tag_open'] = '<li>'; 
		$this->data["pagination_config"]['last_tag_close'] = '</li>'; 		
		$this->data["pagination_config"]['first_url'] = "./?".http_build_query($_GET);
		$this->data["pagination_config"]['suffix'] = "?".http_build_query($_GET);

		// 設定語系
		$this->setLanguage();
	}

	// 設定語系
	public function setLanguage()
	{
		if($this->input->get("lang"))
	    {
	    	$lang = $this->input->get("lang");
	    	$this->session->set_userdata("lang", $lang);
	    }
		elseif($this->session->userdata('lang')) 
	  		$lang = $this->session->userdata('lang');  
		else{
	  		$lang = 'en';
	    	$this->session->set_userdata("lang", $lang);
		}

		$this->lang->load('content', $lang);
	}
	
	public function cap()
	{
		$this->load->helper('captcha');
		
		$vals = array(
	    'img_path'	 => './upload/captcha/',
	    'img_url'	 => './upload/captcha/',
	    'img_width'	 => 100,
	    'img_height' => 39,
	    );
		
		$this->data["cap"] = create_captcha($vals);
		$this->session->set_flashdata('cap_word', $this->data["cap"]["word"]);
		$this->session->set_userdata('cap_word', $this->data["cap"]["word"]);
	}
	
	public function update_click($table,$id)
	{
		if($this->input->is_ajax_request())
		{
			$model = $table.'_m';
			$this->load->model('frontend/'.$model);
			echo $this->$model->click($id);
		}
	}
	
	public function validation()
	{
		$model_name = $this->uri->segments[1] . "_m";
		$model_path = "frontend/" . $model_name;
		
		$this->load->model($model_path);
		
		$model = $this->$model_name;	

		echo $model->validation($this->input->post("name"),$this->input->post("value"),$_POST);
	}
}

class Backend_Info_Controller extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		if($this->data["site_sys"]):
		$this->data['title'] = $this->data["site_sys"]->site_name." 後台管理系統";
		$this->data['icon'] = json_decode($this->data["site_sys"]->back_logo);	
		endif;
		$this->data["msg"] =  $this->session->flashdata('msg');
		$this->data["err_msg"] =  $this->session->flashdata('err_msg');
		$this->data["controller_name"] = $this->uri->rsegments[1];
		$this->data["img_src"] = base_url("upload");
		$this->data["upload_path"] = './upload/';
		$this->data["site_root"] = isset($this->uri->segments[1])?$this->uri->segments[1]:"backend";
		
	}
}

class Backend_Controller extends Backend_Info_Controller
{
	public $view_path;
	public $model;
	public $table;
	public $code_folder;
	public $per_page = 10;

	public function __construct($code_folder)
	{ 
		parent::__construct();

		$this->code_folder = $code_folder;

		if(!$this->session->userdata('user'))
			redirect("backend");
		
		if($code_folder)								
			$this->data["site_root"] = $this->uri->segments[1]."/".$this->uri->segments[2];	
		else
			$this->data["site_root"] = $this->uri->segments[1];
		$this->set_init($code_folder);
	}
	
	public function index($page = 0)
	{	
		$table = $this->table;
		$where = $_GET;
		
		foreach($where as $k=>$v)
	  	{
	   		if(strlen($v)>0)
	   		{ 
	    		if(strpos($k, "_id") === false)
	    		{
	     			switch($k)
	     			{
	        			case "created_on_start":
	       					unset($where[$k]);
					      	$where["$table.created_on >="] = gmt_to_local(strtotime($v), 'UP8');
					       	break;
	    				case "created_on_end":
							unset($where[$k]);
							$where["$table.created_on <="] = gmt_to_local(strtotime($v), 'UP8');
							break;
						case "created_on_date_start":
							unset($where[$k]);
							$where["$table.created_on >="] = $v;
							break;
						case "created_on_date_end":
							unset($where[$k]);
							$where["$table.created_on <="] = $v;
							break;
						case "limit":
							unset($where[$k]);
							$limit = $v;
							break; 
						case "status":
						case "type":
						case "id":
							unset($where[$k]);
							$where["$k ="] = $v;
							break; 
						case "sid":
							unset($where[$k]);
							$where["$k ="] = $v;
							break; 
						case "type":
							unset($where[$k]);
							break; 
						default:
							unset($where[$k]);
							$where["$k like"] = "%$v%";
							break; 
					}
	    		}
				else
				{
					unset($where[$k]);
					$where["$k ="] = $v;
				}				
			}
			else unset($where[$k]);
	  	}
		
		$this->load->library('pagination');
		$config['base_url'] = site_url($this->data["site_root"]."/index/");
		$config['first_url'] = "./?".http_build_query($_GET);
		$config['suffix'] = "?".http_build_query($_GET);
		$config['num_links'] = 5;
		$config['total_rows'] = $this->model->count_by($where);
		$config['per_page'] = $this->per_page; 
		$config['uri_segment'] = 4;		
		$config['next_link'] = '<span aria-hidden="true">»</span><span class="sr-only">Next</span>'; 
		$config['prev_link'] = '<span aria-hidden="true">«</span><span class="sr-only">Previous</span>'; 
		$config['cur_tag_open'] = '<li class="active"><a>'; 
		$config['cur_tag_close'] = '</a></li>'; 
		$config['num_tag_open'] = '<li>'; 
		$config['num_tag_close'] = '</li>'; 
		$config['next_tag_open'] = '<li>'; 
		$config['next_tag_close'] = '</li>'; 
		$config['prev_tag_open'] = '<li>'; 
		$config['prev_tag_close'] = '</li>'; 
		$config['first_tag_open'] = '<li>'; 
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>'; 
		$config['last_tag_close'] = '</li>';
		
		$this->pagination->initialize($config); 
		
		$this->data["pagination"] = $this->pagination->create_links();
	
		$this->model->limit($this->per_page,$page);		

		$this->data["items"] = $this->model->get_many_by($where);
		
		$this->load->view($this->view_path.'index',$this->data);	
	}	
	
	public function import_data($page = 0)
	{
		$model_path = $this->get_model_path($this->code_folder,$this->data["controller_name"]."_import");
				
		$model_name = $this->get_model_name($this->data["controller_name"]."_import");

		if($this->code_folder)
			$this->db->dbprefix($this->code_folder);
		
		$this->load->model($model_name);
		$this->model = $this->$model_name;	
		
		$this->load->library('pagination');
		$config['base_url'] = site_url($this->data["site_root"]."/index/");
		$config['first_url'] = "./?".http_build_query($_GET);
		$config['suffix'] = "?".http_build_query($_GET);
		$config['num_links'] = 5;
		$config['total_rows'] = $this->model->count_by($where);
		$config['per_page'] = $this->per_page; 
		$config['uri_segment'] = 4;		
		
		$this->pagination->initialize($config); 
	
		$this->model->limit($this->per_page,$page);		

		$this->data["items"] = $this->model->get_many_by($where);
		
		$this->load->view($this->view_path.'index',$this->data);	
	}
	
	public function modify($id="", $skip_validation = FALSE, $redirect_root = TRUE)
	{	
		$get = "";
		$page = "";
		if($_GET)
		{
			$data = $_GET;
			$page = isset($data["page"])?"/index/".$data["page"]:"";
			unset($data["page"]);
			$get = "?";
			$i = 1;
			foreach($data as $k=>$v){
				if($i==1)
					$get .= $k."=".$v;
				else
					$get .= "&".$k."=".$v;
					
				$i++;
			}
		}
		
		$this->data['id'] = $id;
		
		$this->data['item'] = $this->model->get($id);
		
		if($_POST)
		{
			
			if($id)
			{
				if($this->model->update($id,$_POST,$skip_validation))
				{
					if($redirect_root){
						$this->session->set_flashdata('msg', '儲存成功');
						redirect($this->data["site_root"].$page.$get);
					}
					else{
						$this->session->set_flashdata('msg', '儲存成功');
						redirect($this->data["site_root"]."/modify/".$id.$get);
					}
				}
				else{
					$this->data["err_msg"] = $this->form_validation->error_array();
				}
			}
			else
			{
				if($this->model->insert($_POST,$skip_validation))
				{	
					$this->session->set_flashdata('msg', '儲存成功');
					
					if(!strrpos($this->agent->referrer(),"modify"))
			        {
				        $this->session->set_flashdata('err_msg', $this->form_validation->error_array());
				        redirect($this->agent->referrer());
			        }
			        
					redirect($this->data["site_root"].$get);
				}
				else
				{
				    if ($this->agent->is_referral())
				    {
				    	
				        if(!strrpos($this->agent->referrer(),"modify"))
				        {
					        $this->session->set_flashdata('err_msg', $this->form_validation->error_array());
					        redirect($this->agent->referrer());
				        }
				    }

					$this->data["err_msg"] = $this->form_validation->error_array();
				}
			}
			
			if(!isset($this->data['item']))
				$this->data['item'] = array();
			else
				$this->data['item'] = (array)$this->data['item'];
				
				
			foreach($_POST as $k=>$v)
			{	
				$this->data['item'][$k] = $v;
			}
			$this->data['item'] = (object)$this->data['item'];
		}		
		$this->load->view($this->view_path.'modify',$this->data);
	}
	
	public function delete($id)
	{
		if($this->model->delete($id))
			$this->session->set_flashdata('msg', '刪除成功');
		else
			$this->session->set_flashdata('err_msg', '刪除失敗');
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function deletes()
	{
		if($this->input->post("ids"))
		{
			foreach($this->input->post("ids") as $v)
				$this->model->delete($v);
		}
		$this->session->set_flashdata('msg', '刪除成功');
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function copys()
	{
		if($this->input->post("ids"))
		{
			foreach($this->input->post("ids") as $v)
				$this->model->copy($v);
		}
		$this->session->set_flashdata('msg', '複製成功');
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function import()
	{
		header("Content-Type:text/html;charset=utf-8");
		$file = str_replace("index.php", "", realpath(SELF))."upload/import/".$this->input->post("import_csv");
		try {
			$this->model->import($file);
			$this->session->set_flashdata('msg', '匯入成功');
		} catch (Exception $e) {
			$this->session->set_flashdata('msg', $e->getMessage());
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function publish()
	{
		try {
			$this->model->publish($this->data["controller_name"]);
			$this->session->set_flashdata('msg', '發佈成功');
		} catch (Exception $e) {
			$this->session->set_flashdata('msg', $e->getMessage());
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function update_field($id)
	{
		if($this->input->is_ajax_request())
		{
			$ids = explode("|", urldecode($id));
			
			foreach($ids as $v)
			{
				if($v)
				{
					if(!$this->model->update($v,$_POST,TRUE))
					{
						echo json_encode(($this->form_validation->error_array()));
					}
				}
			}
		}
		
	}
	
	/* ---------------------------------------custom function--------------------------------------------------------------- */
	
	public function set_init($code_folder)
	{
		$model_path = $this->get_model_path($code_folder,$this->data["controller_name"]);
				
		$model_name = $this->get_model_name($this->data["controller_name"]);
		
		if($code_folder)
			$this->db->dbprefix($code_folder);
		
		$this->view_path = $code_folder."/".$this->data["controller_name"]."/";
		

		
		$this->load->model($model_path);
		
		$this->model = $this->$model_name;		
		
		$this->table = $this->get_table_name($code_folder,$this->data["controller_name"]);

	}
	
	public function get_import_history_status()
	{
		if(substr($this->data["controller_name"],-1)=='y')
			$model = str_replace('y', 'ie', $this->data["controller_name"])  . "s_import_history_m";
		else
			$model = $this->data["controller_name"] . "s_import_history_m";
	
		$this->load_import_status($model);
	}
	
	public function get_import_history_status2()
	{
		$model = $this->data["controller_name"] . "_history_m";
		
		$this->load_import_status($model);
	}
	
	private function load_import_status($model)
	{
		$this->load->model("backend/".$model);
		
		$this->$model->order_by('import_date','desc');
		
		$rows = $this->$model->get_all();
		
		if(count($rows)>0)
			$this->data["publish_status"] = $rows[0]->publish_date==""?"UnPublished":"Published";
		else
			$this->data["publish_status"] = 'no data can publish';
	}
	
	public function get_model_name($model_name)
	{
		return $model_name."_m";
	}
	
	public function get_model_path($code_folder,$model_name)
	{
		return $code_folder."/".$model_name."_m";
	}	
	
	public function get_table_name($code_folder,$model_name)
	{
		return strtolower($code_folder."_".$model_name."s");
	}
	
	public function fileupload()
	{
		if($this->input->is_ajax_request())
		{
			$path = $this->input->post("folder");
			
			$config['upload_path'] = $this->data["upload_path"];
			if($path)
				$config['upload_path'] .= $path."/";	
			
			if(!is_dir($config['upload_path']))
				mkdir($config['upload_path'], 0777);
				
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']	= '204800';
			$config['overwrite'] = false;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload("file_data"))
			{
				$error = array('error' => strip_tags($this->upload->display_errors()));
				echo json_encode($error);
			}
			else
			{
				$upload_data = $this->upload->data();
				
				if($path == "product")
				{
					$this->get_resize_img($config["upload_path"].$upload_data["file_name"],330,450);
// 					$this->get_resize_img($config["upload_path"].$upload_data["file_name"],253,346);
				}
				
				echo json_encode([
				    'initialPreview' => [
				        "<img style='height:160px' src='".base_url("upload/".$path."/".$upload_data["file_name"])."' class='file-preview-image'>",
				    ],
				    'initialPreviewConfig' => [
				        ['caption' => $upload_data["file_name"], 'width' => '120px', 'url' => base_url("backend/".$path.'/filedelete'), 'key' =>$upload_data["file_name"], "extra" => [ "folder" => $path ] ],
				    ],
				    'append' => true // whether to append these configurations to initialPreview.
				                     // if set to false it will overwrite initial preview
				                     // if set to true it will append to initial preview
				                     // if this propery not set or passed, it will default to true.
				]);
			}
		}
	}
	
	public function filedelete()
	{
		echo json_encode([]);
	}
	
	public function do_upload($filename,$path)
	{
	
		
		$config['upload_path'] = $this->data["upload_path"];
		if($path)
			$config['upload_path'] .= $path."/";	
		
		if(!is_dir($config['upload_path']))
			mkdir($config['upload_path'], 0777);
				
		$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|csv';
		$config['max_size']	= '204800';
		$config['overwrite'] = true;
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload($filename))
		{
			$error = array('error' => strip_tags($this->upload->display_errors()));
			echo json_encode($error);
		}
		else
		{
			$upload_data = $this->upload->data();
			$upload_data["name"] = $filename;
			$data = array('upload_data' => $upload_data);
			echo json_encode($data);
		}
	}
	
	public function do_dropzone_upload($path)
	{
		$config['upload_path'] = $this->data["upload_path"];
		if($path)
			$config['upload_path'] .= $path."/";	
		
		if(!is_dir($config['upload_path']))
			mkdir($config['upload_path'], 0777);
				
		$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|csv';
		$config['max_size']	= '204800';
		$config['overwrite'] = false;
		
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload("file"))
		{
			$error = array('error' => strip_tags($this->upload->display_errors()));
			echo json_encode($error);
		}
		else
		{
			$upload_data = $this->upload->data();
			$data["name"] = $upload_data["file_name"];
			$data["size"] = $upload_data["file_size"];
			echo json_encode($data);
		}
	}
}