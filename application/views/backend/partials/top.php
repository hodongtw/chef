<div id="top" class="clearfix">
  <!-- Start App Logo -->
  <div class="applogo" style="font-size:17px;" >
    <p href="#" class="color4 font-w-700">後台管理系統</p>
  </div>
  <!-- End App Logo -->
  <!-- Start Sidebar Show Hide Button -->
  <a href="#" class="sidebar-open-button"><i class="fa fa-bars"></i></a>
  <a href="#" class="sidebar-open-button-mobile"><i class="fa fa-bars"></i></a>
  <!-- End Sidebar Show Hide Button -->
  <!-- Start Searchbox -->
  <!-- End Searchbox -->
  <!-- Start Top Menu -->
  <!-- End Top Menu -->
  <!-- Start Sidepanel Show-Hide Button -->
  <!-- End Sidepanel Show-Hide Button -->
  <!-- Start Top Right -->
  <ul class="top-right">
    <li class="dropdown link">
      <a href="#" data-toggle="dropdown" class="dropdown-toggle profilebox"><img src="" alt=""><?=$this->session->userdata('user')->name?></b><span class="caret"></span></a>
      <ul class="dropdown-menu dropdown-menu-list dropdown-menu-right">
        <li><a href="<?=site_url("backend/login/logout")?>"><i class="fa falist fa-power-off"></i> Logout</a></li>
      </ul>
    </li>
  </ul>
  <!-- End Top Right -->
</div>
<!-- Start Page Loading -->
<div class="loading"><img src="<?=base_url("contents/backend/img/loading.gif")?>" alt="loading-img"></div>
<!-- End Page Loading -->