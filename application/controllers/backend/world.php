<?php 

class world extends Backend_Controller 
{
	public function __construct()
	{
		parent::__construct("backend");			

		$this->per_page = 100;
	}

	public function import_xls()
	{
		$this->load->helper(array('form','url'));
		
		$config['upload_path'] = './upload/import_xls/';
		$config['allowed_types'] = 'xls|xlsx';
		$config['max_size']	= '10240';
		$config['overwrite'] = 'TRUE';

		$this->load->library('upload',$config);
		
		$filed_name = "import_xls";
		
		if (!$this->upload->do_upload($filed_name))
		{
			$error = array('error' => $this->upload->display_errors());
			$this->session->set_flashdata('err_msg', "上傳失敗:".$error);
			redirect(site_url("backend/world"));
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$this->account_import($data["upload_data"]["orig_name"]);
		}
		
	}
	
	function account_import($upload_file)
    {
		$this->load->library('Excel_Factory');
		
		$objPHPExcel = $this->excel_factory->load("./upload/import_xls/$upload_file");
		
		$objPHPExcel->setActiveSheetIndex(0);
		$sheet = $objPHPExcel->getActiveSheet();
		$highestRow = $sheet->getHighestRow(); // 取得總行數
		$ColumnName = $this->getColumnName();
		$err_msg = "";
		$this->in = 0;
		$this->up = 0;
		$this->id = 1;
		if(1)
		{		
			//循環讀取excel文件,讀取一條,插入一條
			for($j=2;$j<=$highestRow;$j++)
			{
				$data = array();
				foreach($ColumnName as $k=>$v)
					$data[$v] = $objPHPExcel->getActiveSheet()->getCell("$k$j")->getValue();
				$resoult = $this->sync_data($data);

				if($resoult === "email")
					$err_msg .= "第".$j."行 email格式錯誤</br>";
				if($resoult === "insert")
					$err_msg .= "第".$j."行 email或phone重複</br>";
			}
		}else
			$this->session->set_flashdata('err_msg', "檔案格式錯誤");
		
		redirect("backend/world");
		
	}
	
	private function sync_data($data)
	{	
		$data["michelin"] = $data["michelin"][0];
		$data["world"] = $this->id;
		if(!$this->model->insert($data))
		{
			return "insert";
		}
		$this->in++;
		$this->id++;
		return TRUE;			
	}

	private function getColumnName()
	{
		$ColumnName = array(
			"A"	=>	"world",
			"B"	=>	"restaurant",
			"C"	=>	"country",
			"D"	=>	"regoin",
			"E"	=>	"michelin",
			"F"	=>	"style",
		);		
		return $ColumnName;
	}
}