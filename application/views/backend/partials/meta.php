<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="keywords" content="" />
  <title>後台管理系統</title>
  <!-- ========== Css Files ========== -->
  <link href="<?=base_url("contents/backend")?>/css/root.css" rel="stylesheet">
  
  <!-- Montserrat -->
  <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
  
  <!-- Roboto -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900,700italic,500italic,400italic,300italic,100italic" rel="stylesheet">
  
  <!-- === Icons === -->
  <link rel="Shortcut Icon" type="image/x-icon" href="<?=base_url("contents/frontend/assets/img/favicon.ico")?>" />
  <!-- Font Awesome -->
  <link href="<?=base_url("contents/backend/css/font-awesome.min.css")?>" rel="stylesheet">
  
  <!-- ================================================
	Main Css
	================================================ -->
  <!-- Bootstrap Main Css File (unedited) -->
  <link href="<?=base_url("contents/backend/css/bootstrap.css")?>" rel="stylesheet">
  
  <!-- Theme Style -->
  <link href="<?=base_url("contents/backend/css/style.css")?>" rel="stylesheet">
  
  <!-- Responsive Style -->
  <link href="<?=base_url("contents/backend/css/responsive.css")?>" rel="stylesheet">
  
  <!-- Shortcuts Css Codes -->
  <link href="<?=base_url("contents/backend/css/shortcuts.css")?>" rel="stylesheet">
  
  <!-- ================================================
	Plugin Css
	================================================ -->
  <!-- Awesome Bootstrap Checkbox -->
  <link href="<?=base_url("contents/backend/css/plugin/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css")?>" rel="stylesheet">
  
  <!-- Bootstrap Select -->
  <link href="<?=base_url("contents/backend/css/plugin/bootstrap-select/bootstrap-select.css")?>" rel="stylesheet">
  
  <!-- Bootstrap Select -->
  <link href="<?=base_url("contents/backend/css/plugin/bootstrap-toggle/bootstrap-toggle.min.css")?>" rel="stylesheet">
  
  <!-- Bootstrap wysihtml5 -->
  <link href="<?=base_url("contents/backend/css/plugin/bootstrap-wysihtml5/bootstrap-wysihtml5.css")?>" rel="stylesheet">
  
  <!-- Summernote -->
  <link href="<?=base_url("contents/backend/css/plugin/summernote/summernote.css")?>" rel="stylesheet">
  <link href="<?=base_url("contents/backend/css/plugin/summernote/summernote-bs3.css")?>" rel="stylesheet">
  
  <!-- Sweet Alert -->
  <link href="<?=base_url("contents/backend/css/plugin/sweet-alert/sweet-alert.css")?>" rel="stylesheet">
  
  <!-- Data Tables -->
  <link href="<?=base_url("contents/backend/css/plugin/datatables/datatables.css")?>" rel="stylesheet">
  
  <!-- Chartist -->
  <link href="<?=base_url("contents/backend/css/plugin/chartist/chartist.min.css")?>" rel="stylesheet">
  
  <!-- Rickshaw -->
  <link href="<?=base_url("contents/backend/css/plugin/rickshaw/rickshaw.css")?>" rel="stylesheet">
  <link href="<?=base_url("contents/backend/css/plugin/rickshaw/detail.css")?>" rel="stylesheet">
  <link href="<?=base_url("contents/backend/css/plugin/rickshaw/graph.css")?>" rel="stylesheet">
  <link href="<?=base_url("contents/backend/css/plugin/rickshaw/legend.css")?>" rel="stylesheet">
  
  <!-- Date Range Picker -->
  <link href="<?=base_url("contents/backend/css/plugin/date-range-picker/daterangepicker-bs3.css")?>" rel="stylesheet">
  
  <!-- Full Calendar -->
  <link href="<?=base_url("contents/backend/css/plugin/fullcalendar/fullcalendar.css")?>" rel="stylesheet">
  
  <!-- FileInput -->
  <link href="<?=base_url("vendor/kartik-v/bootstrap-fileinput/css/fileinput.min.css")?>" rel="stylesheet">
  
  <!-- Arvin Css -->
  <link href="<?=base_url("contents/backend/css/a-style.css")?>" rel="stylesheet">

</head>