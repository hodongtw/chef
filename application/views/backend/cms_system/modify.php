<!DOCTYPE html>
<html lang="en">
<? $this->load->view("backend/partials/meta"); ?>

<body>
  <!-- Start Page Loading -->
  <!-- End Page Loading -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START TOP -->
  <? $this->load->view("backend/partials/top"); ?>
  <!-- END TOP -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START SIDEBAR -->
  <? $this->load->view("backend/partials/sidebar"); ?>
  <!-- END SIDEBAR -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START CONTENT -->
  <div class="content">
    <!-- Start Page Header -->
    <div class="page-header">
      <h1 class="title">系統資訊設定</h1>
      <ol class="breadcrumb">
        <li><a href="<?=site_url("backend/cms_user")?>">系統管理</a></li>
        <li><a href="<?=site_url("backend/cms_system/modify/1")?>">系統資訊設定</a></li>
        <li class="active">編輯</li>
      </ol>
      <!-- Start Page Header Right Div -->
      <!-- End Page Header Right Div -->
    </div>
    <!-- End Page Header -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START CONTAINER -->
    <div class="container-padding">
      <? if($err_msg):?>
      <? foreach($err_msg as $k=>$v):?>
        <div class="kode-alert kode-alert-icon kode-alert-click alert6-light">
          <i class="fa fa-warning"></i>
          <a href="#" class="closed">&times;</a>
          <?=$v?>
        </div>
      <? endforeach;?>
      <? endif;?>              
      <? if($msg):?>
        <div class="kode-alert kode-alert-icon kode-alert-click alert3-light">
          <i class="fa fa-check"></i>
          <a href="#" class="closed">&times;</a>
          <?=$msg?>
        </div>
      <? endif;?>
      <!-- Start Row -->
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-title">
            </div>
            <div class="panel-body">
              <form class="form-horizontal" method="post">
                <div class="form-group">
                  <label class="col-sm-2 control-label form-label"><span style="color:red"> </span>網站名稱</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="site_name" value="<?=$item->site_name?>">
                  </div>
                </div>          
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default submit">送出</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- End Row -->
    </div>
    <!-- END CONTAINER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- Start Footer -->
    <? $this->load->view("backend/partials/footer");?>
    <!-- End Footer -->
  </div>
  <!-- End Content -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START SIDEPANEL -->
  <!-- END SIDEPANEL -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
<? $this->load->view("backend/partials/script");?>
<script>
</script>
</body>
</html>