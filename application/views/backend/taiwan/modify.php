<!DOCTYPE html>
<html lang="en">
<? $this->load->view("backend/partials/meta"); ?>

<body>
  <!-- Start Page Loading -->
  <!-- End Page Loading -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START TOP -->
  <? $this->load->view("backend/partials/top"); ?>
  <!-- END TOP -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START SIDEBAR -->
  <? $this->load->view("backend/partials/sidebar"); ?>
  <!-- END SIDEBAR -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START CONTENT -->
  <div class="content">
    <!-- Start Page Header -->
    <div class="page-header">
      <h1 class="title">台灣排行管理</h1>
      <ol class="breadcrumb">
        <li><a href="<?=site_url("backend/taiwan")?>">內容管理</a></li>
        <li><a href="<?=site_url("backend/taiwan")?>">台灣排行管理</a></li>
        <li class="active">編輯</li>
      </ol>
      <!-- Start Page Header Right Div -->
      <!-- End Page Header Right Div -->
    </div>
    <!-- End Page Header -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START CONTAINER -->
    <div class="container-padding">
      <? if($err_msg):?>
      <? foreach($err_msg as $k=>$v):?>
        <div class="kode-alert kode-alert-icon kode-alert-click alert6-light">
          <i class="fa fa-warning"></i>
          <a href="#" class="closed">&times;</a>
          <?=$v?>
        </div>
      <? endforeach;?>
      <? endif;?>
      <!-- Start Row -->
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-title">
            </div>
            <div class="panel-body">
              <form class="form-horizontal" method="post">
                <div class="form-group">
                  <label class="col-sm-2 control-label form-label"><span style="color:red"></span>Chef</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="<?=$item?$item->name:""?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label form-label">Restaurant</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="restaurant" value="<?=$item?$item->restaurant:""?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label form-label">Style of food</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="style" value="<?=$item?$item->style:""?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label form-label">Taiwan 25</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="taiwan" value="<?=$item?$item->taiwan:""?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label form-label">Asia 50</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="asia" value="<?=$item?$item->asia:""?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label form-label">World 50</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="world" value="<?=$item?$item->world:""?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label form-label">Michelin</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="michelin" value="<?=$item?$item->michelin:""?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label form-label">Speech</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="Speech" value="<?=$item?$item->speech:""?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label form-label">Guest Chef</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="Guest Chef" value="<?=$item?$item->guest:""?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label form-label">Available Days</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="Available Days" value="<?=$item?$item->day:""?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label form-label">Price /1000k USD</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="Price /1000k USD" value="<?=$item?$item->price:""?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label form-label"><span style="color:red"></span>Image</label>
                  <div class="col-sm-10">
                  <input type="hidden" name="image" folder="taiwan" class="ajaxfileuplod" value='<?=$item?$item->image:""?>'/>
                  </div>
                </div>
                <p class="col-sm-offset-2"><span style="color:red">註：建議圖片上傳大小為 300px * 300px 或其等比圖片。</span></p>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default submit">送出</button>
                    <button class="btn btn-light" type="button" onclick="location.href='<?=site_url($site_root)?>'">返回</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- End Row -->
    </div>
    <!-- END CONTAINER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- Start Footer -->
    <? $this->load->view("backend/partials/footer");?>
    <!-- End Footer -->
  </div>
  <!-- End Content -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START SIDEPANEL -->
  <!-- END SIDEPANEL -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
<? $this->load->view("backend/partials/script");?>
<script>
</script>
</body>
</html>