	<input type="hidden" id="site_root" value="<?=site_url($site_root)?>"/>
	<input type="hidden" id="img_src" value="<?=$img_src?>"/>
	
	<!-- New Skin -->
	<script src="<?=base_url('contents/backend/js/jquery.min.js')?>"></script>
	<script src="<?=base_url('contents/backend/js/plugins.js')?>"></script>
	<script src="<?=base_url('contents/backend/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js')?>"></script>
	<script src="<?=base_url('contents/backend/js/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js')?>"></script>
	<script src="<?=base_url('contents/backend/js/date-range-picker/daterangepicker.js')?>"></script>
	<script src="<?=base_url('contents/backend/js/moment/moment.min.js')?>"></script>
	<script src="<?=base_url('contents/backend/js/bootstrap-toggle/bootstrap-toggle.min.js')?>"></script>
	<script src="<?=base_url('contents/backend/js/bootstrap-select/bootstrap-select.js')?>"></script>
	<script src="<?=base_url('contents/backend/js/bootstrap/bootstrap.min.js')?>"></script>
	<script src="<?=base_url('contents/backend/js/summernote/summernote.min.js')?>"></script>
  <script src="<?=base_url('contents/backend/js/sweet-alert/sweet-alert.min.js')?>"></script>
  <script src="<?=base_url('contents/backend/js/kode-alert/main.js')?>"></script>
  <script src="<?=base_url('contents/backend/js/datatables/datatables.min.js')?>"></script>

	<!-- bootstrap-fileinput JS -->
	<script src="<?=base_url('vendor/kartik-v/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js')?>"></script>
  <script src="<?=base_url('vendor/kartik-v/bootstrap-fileinput/js/plugins/sortable.min.js')?>"></script>
	<script src="<?=base_url('vendor/kartik-v/bootstrap-fileinput/js/fileinput.min.js')?>"></script>
	<script src="<?=base_url('vendor/kartik-v/bootstrap-fileinput/js/fileinput_locale_zh-TW.js')?>"></script>
	
	<script src="<?=base_url('contents/backend/js/ajaxupload.js')?>"></script>
	
	<script src="<?=base_url("contents/backend/js/ckeditor/ckeditor.js")?>"></script>
	<script src="<?=base_url("contents/backend/js/ckfinder/ckfinder.js")?>"></script>
	
	
	<script src="<?=base_url('contents/backend/js/custom.js')?>"></script>