<div class="row footer">
  <div class="col-md-6 text-left">
    Copyright © 2017 <a href="http://www.hodong.com.tw/" target="_blank">HoDong</a> All rights reserved.
  </div>
  <div class="col-md-6 text-right">
    Design and Developed by <a href="http://www.hodong.com.tw/" target="_blank">HoDong</a>
  </div>
</div>